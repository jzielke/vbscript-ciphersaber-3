## CipherSaber Tests

The files in this folder are useful for testing a CipherSaber to make sure it is functioning correctly.

 * cstest1.cs1 - Example CipherSaber-1 file.
 * cstest2.cs1 - Another CipherSaber-1 test file.
 * cstest1.cs2 - Example CipherSaber-2 file.
 * cstest.txt - Passwords and settings for the .cs1 and .cs2 test files.
 * Wikipedia.txt - How to use the [test vectors](https://en.wikipedia.org/wiki/RC4#Test_vectors) from the Wikipedia to test RC4 compatibility.
 * RFC6229.txt - How to use [RFC 6229](https://tools.ietf.org/html/rfc6229) to test RC4-drop[n] compatibility.
