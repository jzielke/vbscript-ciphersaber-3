https://en.wikipedia.org/wiki/RC4#Test_vectors
https://en.wikipedia.org/w/index.php?title=RC4&oldid=805203163#Test_vectors

The Wikipedia test vectors test plain RC4 compatibility.

Key	Plaintext	Ciphertext
----------------------------------------------------
Key	Plaintext	BBF316E8D940AF0AD3
Wiki	pedia		1021BF0420
Secret	Attack at dawn	45A01F645FC35B383552544B9BF5

CipherSaber settings:
Operation:		Encrypt
Hex Input/Output:       Checked
Preset:			Custom
IV Length:		0
Rounds:			1
Skip Bytes:		0

Create a text file and put one of the example plaintexts in it.  Encrypt 
the file with the key for your test text.  Open the output file in a text 
editor.