# README #

### CipherSaber-3 ###
![CipherSaber-3 on Windows XP](CipherSaber-3.png)

* Compatible with [CipherSaber-1](http://ciphersaber.gurus.org/faq.html#getrc4) and [CipherSaber-2](http://ciphersaber.gurus.org/faq.html#cs2), as well as adds [key hashing](https://en.wikipedia.org/wiki/RC4#Fluhrer.2C_Mantin_and_Shamir_attack) (disabled by default) and [RC4-Drop[n]](http://www.users.zetnet.co.uk/hopwood/crypto/scan/cs.html#RC4-drop).
* HTML Application should run in most versions of Windows (from 95 on).

### Why? ###
[CipherSaber](http://ciphersaber.gurus.org/) is a good exercise for programmers and a useful tool for encryption, but what about non-programmers?  This is a simple GUI for family and friends of CipherSaber authors to use.

### What is this? ###
The purpose of CipherSaber is encryption that you can memorize.  The algorithm is so simple that it is possible to write a program that can encrypt and decrypt CipherSaber compatible files from memory.  If you can write software this script is not for you.  Take a moment and write your own.  This is an easy to use script for non-programmers to use.  To learn more you will have to use the [Internet Archive](https://web.archive.org/) as some of the links are gone.

The algorithm:  
[Applied Cryptography, by Bruce Schneier, Wiley, 1996](http://www.cs.tufts.edu/comp/250P/classpages/RC4.html)

The official pages:  
The domain changed from .com to .org so all of the inter-page links are broken.  There are only 3 pages.  
[CipherSaber Main Page](http://ciphersaber.gurus.org/) The page that started it all.  
[CipherSaber FAQ](http://ciphersaber.gurus.org/faq.html) The algorithm is explained on this page.  There are also some sample files and debugging info helpful for writing your own.  
[CipherSaber Cryptanalysis](http://ciphersaber.gurus.org/cryptanalysis.html) Some weaknesses of the algorithm are described here.  Not all of the information is easy to follow but the suggestions on how to use the software are very useful.

Live page:  
[The Arcfour Stream Cipher](http://nullprogram.com/blog/2008/08/09/) A description of the algorithm and a visual explanation of the purpose of the Initialization Vector.

Dead (but very helpful) pages:  
[Marek Jedlinski's CipherSaber Page](http://web.archive.org/web/20000419170908/http://www.lodz.pdi.net/~eristic/free/ciphersaber.html) A very nice description of the how and why of CipherSaber.  This is a more verbose, and IMO, easier to understand description of the algorithm.  
[Jay Holovacs's CipherSaber Page](http://web.archive.org/web/20000301191622/http://www.freespeech.org:80/freethought/CipherSaber.htm) Another description of the algorithm and an intersting password sharing idea.  **mkpads.vbs and xor.hta were added for this purpose.**

### Configuration ###
CipherSaber-3.hta is a script and can be edited using notepad.exe.  

There are 3 window sizes near the top of the screen.  If you have scrollbars try a different size.  Just delete the ' mark in front of the size you want to try and add a ' mark in front of the old size.  Press F5 in the program to try the new size.  You do not have to close and re-open it.

IV Length defaults to 10 bytes.  It can be changed while the program is running.  If you use a different IV length you can edit the program to change the default.

There are 3 presets for CipherSaber-1 (decrypt only), CipherSaber-2, and CipherSaber-3 (RC4-Drop[n]).  The default is CipherSaber-3, which will not be decypherable to anyone using CipherSaber 1 or 2.  You can select a different preset while the program is running and you can change the default by editing the program.

There is a custom preset.  If you make that the default (comment all 3 of the other presets) make sure you set the values below it.

cs1encrypt - With this set to False (default) you can only decrypt CipherSaber-1 messages.  Change this to True to enable encrypting CipherSaber-1 messages for testing.

allowhashing - With this set to False (default) hashing the passphrase and IV together is not an option.  I feel that hashing violates the spirit of CipherSaber but it is a very well tested way to keep messages safe.  .NET 1.1 needs to be installed for hashing to work.

## Presets ##
CipherSaber-1: Rounds: 1, Skip Bytes: 0, Hash: none  
CipherSaber-2: Rounds: 20, Skip Bytes: 0, Hash: none  
CipherSaber-3: Rounds: 20, Skip Bytes: 12, Hash: none

arcfour: Rounds: 1, Skip Bytes: 0, Hash: none, IV Length: 0  
RC4-Drop[n]: Rounds: 1, Skip Bytes: n/256, Hash: none, IV Length: 0

arcfour - This setting is useful for checking [RC4 test vetors](https://en.wikipedia.org/wiki/RC4#Test_vectors).  If the wiki is vandalized use [this link](https://en.wikipedia.org/w/index.php?title=RC4&oldid=805203163#Test_vectors).  
RC4-Drop[n] - Use Skip Bytes 12 to skip 3072 bytes, or 3 to skip 768 bytes.  [RSA](https://web.archive.org/web/20110811031655/https://www.rsa.com/rsalabs/node.asp?id=2009) (another Internet Archive link) suggests a minimum of 256 bytes skipped (Skip Bytes of 1).

