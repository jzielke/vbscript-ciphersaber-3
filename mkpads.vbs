Option Explicit

Dim length, block, files, filename, namelength, a, b, c, i, r, lngSeed, lngCarry, hexstring, objRandom, prngs

files = 4
length = 256
block = 64
namelength = 4

' Hex means multiply byte lengths by 2
length = length * 2
block = block * 2
namelength = namelength * 2

prngs = Array("GetGUID", "GetMWC", "GetLCG")	' DotNetRandom added later if loadable
CheckDotNet

lngSeed = Fix((Timer * (2^32)) / (24 * 60 * 60))
lngCarry = 48313

Rnd -1
Randomize lngSeed

Sub CheckDotNet
  On Error Resume Next
  Set objRandom = CreateObject("System.Random")

  If Err.Number = 0 Then
    ReDim Preserve prngs(UBound(prngs)+1)
    prngs(UBound(prngs)) = "GetDotNetRandom"
  End If
End Sub

Function GetGUID
  ' GUID
  Dim guid
  guid = CreateObject("Scriptlet.TypeLib").Guid
  GetGUID = Mid(guid, 2, 8) & Mid(guid, 11, 4) & Mid(guid, 17, 3) &  Mid(guid, 22, 3) & Mid(guid, 26, 12)
End Function

Function GetDotNetRandom
  ' .NET Random
  GetDotNetRandom = Right("0000000" & Hex(objRandom.Next()), 7)
End Function

Function MWC(ByRef lngX, ByRef lngC)
  ' Multiply With Carry
  ' http://www.rlmueller.net/Programs/MWC32.txt
  Dim S_Hi, S_Lo, C_Hi, C_Lo
  Dim F1, F2, F3, T1, T2, T3

  Const A_Hi = 63551
  Const A_Lo = 25354
  Const M = 4294967296
  Const H = 65536

  S_Hi = Fix(lngX / H)
  S_Lo = lngX - (S_Hi * H)
  C_Hi = Fix(lngC / H)
  C_Lo = lngC - (C_Hi * H)
  F1 = A_Hi * S_Hi
  F2 = (A_Hi * S_Lo) + (A_Lo * S_Hi) + C_Hi
  F3 = (A_Lo * S_Lo) + C_Lo
  T1 = Fix(F2 / H)
  T2 = F2 - (T1 * H)
  lngX = (T2 * H) + F3
  T3 = Fix(lngX / M)
  lngX = lngX - (T3 * M)
  lngC = Fix((F2 / H) + F1)
  MWC = lngX
End Function

Function GetMWC
  lngSeed = MWC(lngSeed, lngCarry)
  GetMWC = Right("00000000" & MyHex(lngSeed), 8)
End Function

Function LCG(ByRef lngX)
  ' Linear Congruential Generator
  ' https://groups.google.com/d/msg/microsoft.public.scripting.vbscript/yFK7mrlrGPg/StQxNPo8n3cJ
  Dim L1, L2, L3, S_Hi, S_Lo

  Const A_Hi = 8986055
  Const A_Lo = 3222621
  Const C_Hi = 4611540
  Const C_Lo = 9781439
  Const M = 100000000000000	' 10^14
  Const H = 10000000		' 10^7

  S_Hi = Fix(lngX / H)
  S_Lo = lngX - (S_Hi * H)
  L2 = (A_Hi * S_Lo) + (A_Lo * S_Hi)
  L3 = A_Lo * S_Lo
  L1 = Fix(L2 / H)
  L2 = L2 - (L1 * H)
  LCG = ((L2 + C_Hi) * H) + L3 + C_Lo
  L1 = Fix(LCG / M)
  LCG = LCG - (L1 * M)
End Function

Function GetLCG
  lngSeed = LCG(lngSeed)
  GetLCG = Right("00000000000" & MyHex(lngSeed), 11)
End Function

Function MyHex(ByVal TempDec)
  ' http://visualbasic.ittoolbox.com/groups/technical-functional/visualbasic-l/vb60-hex-function-overflow-error-2744358#M2746495
  Dim TNo
  Do
    TNo = TempDec - (Fix(TempDec / 16) * 16)
    If TNo > 9 Then
      MyHex = Chr(55 + Tno) & MyHex
    Else
      Myhex = TNo & MyHex
    End If
    TempDec = Fix(TempDec / 16)
  Loop Until (TempDec = 0)
End Function

Sub WriteHex(file, hexstring)
  Dim i, wts, wfso
  Set wfso = CreateObject("Scripting.FileSystemObject")
  Set wts = wfso.CreateTextFile(file)
  For i = 1 to Len(hexstring) Step 2
    wts.Write(Chr(CInt("&H" & Mid(hexstring, i, 2))))
  Next
End Sub

Function StringXor(ByVal x, ByVal y)
  Dim w, z
  For z = 1 to Len(x) Step 2
    w = w & ("&H" & Mid(x, z, 2) Xor "&H" & Mid(y, z, 2))
  Next
  StringXor = w
End Function

Function RunByRef(func)
  Dim f : Set f = GetRef(Func)
  RunByRef = f
End Function

Function Choose2(arr)
  Dim j, k
  j = Int((UBound(arr)+1)*Rnd)
  k = Int((UBound(arr)+1)*Rnd)
  Choose2 = Array(arr(j), arr(k))
End Function

For i = 1 to files
  filename = ""
  Do While Len(filename) < namelength
    filename = filename & RunByRef(prngs(Int(Rnd*(UBound(prngs)+1))))
  Loop
  filename = Left(filename, namelength) & ".bin"
  c = ""
  Do While Len(c) < length
    r = Choose2(prngs)
    a = ""
    Do While Len(a) < block
      a = a & RunByRef(r(0))
    Loop
    a = Left(a, block)
    If r(0) = r(1) Then
      c = c & a
    Else
      b = ""
      Do While Len(b) < block
        b = b & RunByRef(r(1))
      Loop
      b = Left(b, block)
      c = c & StringXor(a, b)
    End If
  Loop
  c = Left(c, length)
  WriteHex filename, c
Next
